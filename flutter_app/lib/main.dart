import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fisrt Assignment',
      theme: ThemeData(
        primarySwatch: Colors.red
      ),
      home: MyHomePage(
        title: 'HomeWork 3',
      )
    );
  }
}

class likeWidget extends StatefulWidget {

  @override
  _LikeWidgetState createState() => _LikeWidgetState();
}

class _LikeWidgetState extends State<likeWidget> {
  bool _isLiked = false;
  int _likeCount = 16;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        LogoApp(),
        Container(
          padding: EdgeInsets.all((0)),
          child: IconButton(
            icon: (_isLiked ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            color: Colors.red,
            onPressed: _toggleLike,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_likeCount'),
          ),
        )
      ],
    );
  }

  void _toggleLike() {
    setState(() {
      if (_isLiked) {
        _isLiked = false;
        _likeCount -= 1;
      } else {
        _isLiked = true;
        _likeCount += 1;
      }
    });
  }
}

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 15), vsync: this);
    animation = Tween<double>(begin: 0, end: 200).animate(controller)
      ..addListener(() {
        setState(() {

        });
      });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin:  EdgeInsets.symmetric(vertical: 10),
        height: animation.value,
        width: animation.value,
        child: FlutterLogo(),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    Widget titleWidget = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(5),
            child: Text("Champions League - Paris Saint Germain",
              style: TextStyle(
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 5),
            child: Text(
              'Neymar left in tears after PSG suffer Champions League\nfinal heartbreak (08/25/2020)',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
          ),
        ],
      ),
    );

    Widget textOverviewWidget = Container(
      padding: EdgeInsets.all(10),
      child: Text(
        'Estádio do Sport Lisboa e Benfica in Lisbon (Portugal) hosted the 2019/20 UEFA Champions League final on Sunday 23 August at 9 p.m. '
            'Also named Estádio da Luz, the lair of 37-time Portuguese champion Benfica was rebuilt for UEFA EURO 2004 and was home to the 2014'
            ' UEFA Champions League final, Real Madrid\'s 4-1 victory after extra time against neighbors Atlético for La Décima des Merengues. '
            'Paris landed the first ticket to the final (the first final in the club\'s history) by winning against Leipzig in the semi-final (3-0). '
            'He faced Bayern, who eliminated Lyon in the second semi-final.'
      ),
    );

    Widget textStatWidget = Container(
      padding: EdgeInsets.all(10),
      child: Text(
          'Paris Saint-Germain star Neymar was left in tears after his side suffered a 1-0 Champions League final loss to Bayern Munich in Lisbon.'
              'The Ligue 1 champions succumbed to their German opponents due to a Kingsley Coman header shortly before the hour mark, and the 28-year-old'
              'was unable to inspire a comeback as Bayern kept the French outfit at arm’s length. eymar, who was consoled in the dugout by sporting '
              'director Leonardo at full-time, had one of the outstanding opportunities in the opening period of the game but was denied by an inspired '
              'Manuel Neuer, who also stopped a tame effort from Kylian Mbappe before the break.'
      ),
    );

    Column _buildButtonWidget(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 3),
            child: Text(label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
            ),
          ),
        ],
      );
    }

    Widget buttonOverviewWidget = Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Container(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildButtonWidget(Colors.red, Icons.not_listed_location, 'Location: Lisbonne'),
              _buildButtonWidget(Colors.red, Icons.looks_one, 'Background: 1st Final'),
              _buildButtonWidget(Colors.red, Icons.euro_symbol, 'Reward: 134 M€'),
            ],
          ),
        ],
      ),
    );

    Widget buttonStatWidget = Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Container(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildButtonWidget(Colors.red, Icons.person_add, 'Top player: NeymarJR'),
              _buildButtonWidget(Colors.red, Icons.person_outline, 'Flop player: Di Maria'),
              _buildButtonWidget(Colors.red, Icons.star, 'Huge opportunity: 4'),
            ],
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: <Widget>[
          Image.asset('image/psg.jpg',
            width: 600,
            height: 240,
            fit: BoxFit.cover,
          ),
          titleWidget,
          buttonOverviewWidget,
          textOverviewWidget,
          buttonStatWidget,
          textStatWidget,
          likeWidget(),
        ],
      ),
    );
  }
}
